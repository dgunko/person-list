'use strict';
function personEditView () {
  var editView = $('#edit-view'),
      previewButton = $('.preview'),

      editName = $("input[name='name']"),
      editLastName = $("input[name='lastName']"),
      editGender = $("input[name='gender']"),
      editCity = $("input[name='city']"),
      editCountry = $("input[name='country']");

  mediator.installTo(editView);
  editView.subscribe('customizePerson', function () {
    editView.removeClass('hide');
  });
  editView.subscribe('showEdit', function () {
    editView.removeClass('hide');
  });
  editView.subscribe('showPreview', function () {
    editView.addClass('hide');
  });

  this.initPersonFields = function (curentPerson) {
    editName.val(curentPerson['name']);
    editLastName.val(curentPerson['lastName']);
    editGender.val(curentPerson['gender']);
    editCity.val(curentPerson['city']);
    editCountry.val(curentPerson['country']);
  };

  mediator.installTo(previewButton);
  previewButton.on('click', function () {
    var editedInfo = temporaryPersonInfo();
    personPreviewView(editedInfo);
    
    previewButton.publish('showPreview');
  });

  function temporaryPersonInfo () {
    var editedInfo = {
      name: editName.val(),
      lastName: editLastName.val(),
      gender: editGender.val(),
      city: editCity.val(),
      country: editCountry.val()
    };
    return editedInfo;
  }

  this.updatedPersonInfo = function () {
    var editedInfo = {
      name: editName.val(),
      lastName: editLastName.val(),
      gender: editGender.val(),
      city: editCity.val(),
      country: editCountry.val()
    };
    return editedInfo;
  }
}