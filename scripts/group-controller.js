'use strict';
function groupController () {
  var students = new Group();
  students.initGroupName('Dp-086');

  students.initGroupList('Alexander', 'Denga', 'male');
  students.initGroupList('Alexander', 'Poltorak', 'male');
  // students.initGroupList('Artem', 'Yekzarkho', 'male');
  // students.initGroupList('Yevgeniya', 'Krischik', 'female');
  // students.initGroupList('Maryan', 'Kotsylovskiy', 'male');
  // students.initGroupList('Dmitry', 'Gunko', 'male');
  // students.initGroupList('Dmitry', 'Sashkov', 'male');
  // students.initGroupList('Yuliya', 'Luryeva', 'female');

  groupView(students);

  mediator.installTo(students);
  students.subscribe('submit', function () {
    $('#container').empty();
    groupView(students);
  });
}