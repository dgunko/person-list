'use strict';
function groupView (students) {
  var person,
      customizeButton,
      userForms = new personEditView();

  $.each(students.displayGroupList(), function (i, value) {
    person = new personView();
    $('#container').append(person.render(value.getPersonInfo()));
    
    customizeButton = $('.showEdit')[i];
    mediator.installTo(customizeButton);
    $(customizeButton).on('click', function() {
      customizeButton.publish('customizePerson');
      userForms.initPersonFields(value.getPersonInfo());
    });

    $(customizeButton).on('click', function() {
      customizeButton.subscribe('submit', function () {
        value.initPersonInfo(
          userForms.updatedPersonInfo()['name'],
          userForms.updatedPersonInfo()['lastName'],
          userForms.updatedPersonInfo()['gender'],
          userForms.updatedPersonInfo()['city'],
          userForms.updatedPersonInfo()['country']
        );
      });
    });

  });
}