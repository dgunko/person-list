'use strict';
var tpl = {};

tpl['person'] = _.template([
  '<div class="person">',
    '<h1>Student: <%= name %> <%= lastName %> </h1>',
    '<ul>',
      '<li><%= name %></li>',
      '<li><%= lastName %></li>',
      '<li><%= gender %></li>',
    '</ul>',
    '<button class="showEdit">Show more</button>',
  '<div>'
].join(' '));

tpl['preview'] = _.template([
  '<p>Name: <%= name %></p>',
  '<p>Last Name: <%= lastName %></p>',
  '<p>Gender: <%= gender %></p>',
  '<p>City: <%= city %></p>',
  '<p>Country: <%= country %></p>',
  '<button class="previewEdit">Edit</button>',
  '<button class="previewSubmit">Update</button>'
].join(' '));