'use strict';
function Person () {
  var name,
      lastName,
      gender,
      city,
      country;

  this.initPersonInfo = function (_name, _lastName, _gender, _city, _country) {
    name = _name;
    lastName = _lastName;
    gender = _gender;
    city = _city;
    country = _country;
  };

  this.getPersonInfo = function () {
    var userData = {
      name: name,
      lastName: lastName,
      gender: gender,
      city: city,
      country: country
    };
    return userData;
  };
}