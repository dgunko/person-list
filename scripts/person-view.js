'use strict';
function personView () {
  this.render = function (person) {
    var paragraph = $('<div></div>').append(tpl['person']({ 
        name: person['name'],
        lastName: person['lastName'],
        gender: person['gender']
    }));

    return paragraph;
  };
}