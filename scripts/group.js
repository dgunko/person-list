'use strict';
function Group () {
  var groupName,
      list = [],
      person;

  this.initGroupName = function (_groupName) {
    groupName = _groupName;
  };

  this.shouGroupName = function () {
    return groupName;
  };

  this.initGroupList = function (name, lastName, gender, city, country) {
    person = new Person();
    person.initPersonInfo(name, lastName, gender, city, country);
    list.push(person);
  };

  this.displayGroupList = function () {
    return list;
  };
}