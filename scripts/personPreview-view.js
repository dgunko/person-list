'use strict';
function personPreviewView (editedInfo) {
  var previewView = $('#preview-view'),
      previewSubmit,
      previewEdit;
  
  mediator.installTo(previewView);
  previewView.subscribe('showPreview', function () {
    previewView.removeClass('hide');
  });
  previewView.subscribe('showEdit', function () {
    previewView.addClass('hide');
  });
  previewView.subscribe('submit', function () {
    previewView.addClass('hide');
  });

  previewView.empty();
  previewView.append(tpl['preview']({ 
      name: editedInfo['name'],
      lastName: editedInfo['lastName'],
      gender: editedInfo['gender'],
      city: editedInfo['city'],
      country: editedInfo['country']
  }));

  previewEdit = $('.previewEdit');
  mediator.installTo(previewEdit);
  previewEdit.on('click', function () {
    previewEdit.publish('showEdit');
  });

  previewSubmit = $('.previewSubmit');
  mediator.installTo(previewSubmit);
  previewSubmit.on('click', function () {
    previewSubmit.publish('submit');
  });
}